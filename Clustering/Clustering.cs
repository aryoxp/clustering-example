using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Clustering
{
    public partial class FormClustering : Form
    {
        public FormClustering()
        {
            InitializeComponent();
            InitialValue();
        }

        // Variabel untuk menyimpan data matrix jarak yang pertama
        Matrix Data;

        /// <summary>
        /// Method untuk melakukan inisialiasai nilai variabel data matrix jarak
        /// </summary>
        private void InitialValue()
        {
            udJumlah.Value = 5;

            Data = new Matrix(5, 5); // matrix jarak

            // isi data matrix jarak
            Data[1, 0] = 9; 
            Data[2, 0] = 3; 
            Data[3, 0] = 6; 
            Data[4, 0] = 11; 
            Data[2, 1] = 7; 
            Data[3, 1] = 5; 
            Data[4, 1] = 10; 
            Data[3, 2] = 9; 
            Data[4, 2] = 2; 
            Data[4, 3] = 8;

            // mirroring data matrix jarak
            for (Int32 i = 0; i < Data.NoCols - 1; i++)
            {
                for (Int32 j = 1; j < Data.NoRows; j++)
                {
                    if (i >= j) continue;
                    Data[i, j] = Data[j, i];
                }
            }
            // tampilkan datanya;
            ShowMatrix(Data);
        }

        /// <summary>
        /// Method Event untuk membersihkan Debug Message
        /// </summary>
        private void btClear_Click(object sender, EventArgs e)
        {
            tbOutput.Clear();
        }

        #region Debug
        /// <summary>
        /// Koleksi method untuk menampilkan pesan/nilai pada Debug Message
        /// </summary>
        private void Debug(String label, Double[] data)
        {
            List<String> DataString = new List<String>();
            foreach (Double d in data)
            {
                DataString.Add(String.Format("{0:0.#####}", d));
            }
            tbOutput.SelectionFont = new Font("Tahoma", (float)8, FontStyle.Bold);
            tbOutput.SelectedText += " " + label + "\r\n";
            tbOutput.SelectionFont = new Font("Tahoma", (float)8, FontStyle.Regular);
            tbOutput.SelectedText += " " + String.Join(", ", DataString.ToArray()) + "\r\n\r\n";
        }

        private void Debug(String label, String data)
        {
            tbOutput.SelectionFont = new Font("Tahoma", (float)8, FontStyle.Bold);
            tbOutput.SelectedText += " " + label + "\r\n";
            tbOutput.SelectionFont = new Font("Tahoma", (float)8, FontStyle.Regular);
            tbOutput.SelectedText += " " + data + "\r\n\r\n";
        }

        private void Debug(String label, Double data)
        {
            tbOutput.SelectionFont = new Font("Tahoma", (float)8, FontStyle.Bold);
            tbOutput.SelectedText += " " + label + "\r\n";
            tbOutput.SelectionFont = new Font("Tahoma", (float)8, FontStyle.Regular);
            tbOutput.SelectedText += " " + String.Format("{0:0.#####}", data) + "\r\n\r\n";
        }

        private void Debug(Matrix m, String name)
        {
            tbOutput.SelectionFont = new Font("Tahoma", (float)8, FontStyle.Bold);
            tbOutput.SelectedText += " " + name + "\r\n";
            tbOutput.SelectionFont = new Font("Tahoma", (float)8, FontStyle.Regular);
            tbOutput.SelectedText += m.ToString() + "\r\n\r\n";

        }
        #endregion

        #region ShowMatrix(Matrix m) Display Matrix to DataGridView
        /// <summary>
        /// Method untuk menampilkan matrix jarak ke komponen DataGridView
        /// </summary>
        /// <param name="m"></param>
        private void ShowMatrix(Matrix m)
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            
            // Prepare column to DataSet
            for (Int32 i = 0; i < m.NoCols; i++)
            {
                DataColumn dc = new DataColumn(i.ToString());
                dt.Columns.Add(dc);
            }

            // Memasukkan data ke DataRow
            for (Int32 i = 0; i < m.NoRows; i++)
            {
                List<Object> col = new List<Object>();
                for (Int32 j = 0; j < m.NoCols; j++)
                {
                    Double val = m[i, j];
                    col.Add(val.ToString());
                }
                dt.Rows.Add(col.ToArray());
            }

            // Masukkan data ke DataSet
            ds.Tables.Add(dt);

            // Inisialisasi label header untuk setiap kolom
            for (Int32 i = 0; i < ds.Tables[0].Columns.Count; i++)
            {
                String[] label = new String[]{"A","B","C","D","E","F","G","H","I","J","K","L","M"};
                ds.Tables[0].Columns[i].ColumnName = label[i];
            }
            
            // Tampilkan data matrix jarak pada DataGridView
            dgData.DataSource = ds.Tables[0];
            for (Int32 i = 0; i < dgData.Columns.Count; i++) dgData.Columns[i].Width = 50;

        }
        #endregion

        #region DataGrid Modifier
        /// <summary>
        /// Method event ketika jumlah elemen diubah
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void udJumlah_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                // Matrix untuk menyimpan data dengan dimensi yang baru
                Matrix temp;

                // Jika dimensi matrix jarak diperbesar, 
                // Copy data ke matrix dengan dimensi yang baru
                if (udJumlah.Value > Data.NoRows)
                {
                    temp = new Matrix(Data.NoRows + 1, Data.NoCols + 1);
                    for (Int32 i = 0; i < Data.NoRows; i++)
                    {
                        for (Int32 j = 0; j < Data.NoCols; j++)
                        {
                            temp[i, j] = Data[i, j];
                        }
                    }
                }
                else
                // Jika dimensi matrix jarak diperkecil, 
                // Copy data ke matrix dengan dimensi yang baru
                {
                    temp = new Matrix(Data.NoRows - 1, Data.NoCols - 1);
                    for (Int32 i = 0; i < temp.NoRows; i++)
                    {
                        for (Int32 j = 0; j < temp.NoCols; j++)
                        {
                            temp[i, j] = Data[i, j];
                        }
                    }
                }

                // Simpan matrix yang baru ke matrix data
                Data = temp;
                
                // Tampilkan matrixnya
                ShowMatrix(Data);
            }
            catch { };
        }

        /// <summary>
        /// Method event ketika data matrix jarak pada DataGrid diubah.
        /// </summary>
        private void dgData_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                // Parse nilai yang baru
                String cellValue = (String)dgData.Rows[e.RowIndex].Cells[e.ColumnIndex].Value;
                Double NewValue = Double.Parse(cellValue);
                // Update nilai yang lama dengan yang baru
                Data[e.RowIndex, e.ColumnIndex] = NewValue;
            }
            catch { }

            // Tampilkan matrixnya
            ShowMatrix(Data);
        }
        #endregion


        /// <summary>
        /// Method event ketika tombol "Proses" di klik
        /// </summary>
        private void btProses_Click(object sender, EventArgs e)
        {
            // bersihkan Debug Message
            tbOutput.Clear();

            #region manual
            /*
            
            Int32[] minimumPos = GetMinimumPos(Data);
            Double minVal = Data[minimumPos[0], minimumPos[1]];
            Debug("Minimum", minVal);
            Debug("Posisi", minimumPos[0] + "," + minimumPos[1]);
            
            Matrix temp = Data;
            temp = new Matrix(temp.NoRows - 1, temp.NoRows - 1);
            Matrix tes = new Matrix(temp.NoRows, temp.NoRows);
            
            m = Data[2, 0];
            n = Data[4, 0];
            if (m > n) x = n; else x = m;
            baru[1, 0] = x;

            x = 0;
            m = Data[2, 1];
            n = Data[4, 1];
            if (m > n) x = n; else x = m;
            baru[2, 0] = x;

            x = 0;
            m = Data[2, 3];
            n = Data[4, 3];
            if (m > n) x = n; else x = m;
            baru[3, 0] = x;

            x = 0;
            baru[2, 1] = Data[1, 0];
            baru[3, 1] = Data[3, 0];

            baru[3, 2] = Data[3, 1];
            Debug(baru, "Matrix baru");
            */
            #endregion

            // tampilkan matrix data
            Debug(Data, "Matrix Data");

            // lakukan iterasi pertama pada matrix data
            Matrix temp = Iterate(Data);
            Debug(temp, "Matrix iterasi 1 ");

            // lakukan iterasi kedua pada matrix hasil iterasi sebelumnya
            temp = Iterate(temp);
        }

        #region Iterasi Clustering Multivariate
        /// <summary>
        /// Function iterasi clustering multivariate terhadap matrix jarak yang diberikan
        /// </summary>
        /// <param name="mtx">Matrix jarak yang akan diiterasi ( m x m )</param>
        /// <returns>Matrix hasil iterasi ((m-1) x (m-1)) </returns>
        private Matrix Iterate(Matrix mtx)
        {
            // Cari posisi nilai minimum dari matrix jarak
            Int32[] minimumPos = GetMinimumPos(mtx);

            // Ambil nilai data minimum pada posisi nilai data yang minimum
            // dari matrix jarak
            Double minVal = mtx[minimumPos[0], minimumPos[1]];

            // Tampilkan nilai minimumnya
            Debug("Minimum", minVal);

            // Tampilkan posisi nilai minimum pada matrix jarak
            Debug("Posisi", minimumPos[0] + "," + minimumPos[1]);

            // Lakukan penyusunan matrix jarak yang baru sebagai hasil iterasi
            Matrix temp = new Matrix(mtx.NoRows - 1, mtx.NoCols - 1);
            Double x = 0; Double m = 0; Double n = 0;
            Int32 k = 0; Int32 l = 0; Int32 i = 0;
            for (Int32 j = 0; j < temp.NoCols - 1; j++)
            {
                k = i;
                l = j;

                for (i = 1; i < temp.NoRows; i++)
                {
                    if (j >= i) continue;
                    
                    // cari nilai minimum pada kolom pertama matrix jarak yang baru
                    // yang diperoleh dari gabungan baris dan kolom pada posisi nilai minimum
                    // matrix jarang yang sebelumnya
                    if (j == 0) 
                    {
                        if (i - 1 == minimumPos[0] || i - 1 == minimumPos[1]) k++;
                        m = mtx[minimumPos[0], k];
                        n = mtx[minimumPos[1], k];
                        if (m > n) x = n; else x = m;
                        temp[i, j] = x;
                        k++;
                    }
                    // cari nilai antar kolom dan baris 
                    // selain dari kolom dan baris nilai minimum
                    // pada matrix jarak sebelumnya.
                    else
                    {
                        k = i;
                        if (l - 1 == minimumPos[0] || l - 1 == minimumPos[1]) l++;
                        if (k - 1 == minimumPos[0] || k - 1 == minimumPos[1]) k++;
                        temp[i, j] = mtx[k - 1, l - 1];
                    }
                }
                l++;

            }
            // kembalikan matrix hasil iterasinya
            return temp;

        }
        #endregion


        #region Mencari Posisi Nilai Minimum pada Matrix
        /// <summary>
        /// Fungsi untuk mendapatkan posisi baris dan kolom untuk nilai 
        /// yang minimum pada matrix yang diberikan
        /// </summary>
        /// <param name="m">Matrix yang akan dicari posisi nilai minimumnya</param>
        /// <returns>Array posisi baris dan kolom nilai minimum dari Matrix m</returns>
        private Int32[] GetMinimumPos(Matrix m)
        {
            Int32[] pos = new Int32[2];
            Int32 i = 1;
            Int32 j = 0;

            // Set posisi minimum awal pada baris 1 kolom 0
            Double minimum = m[1, 0];
            pos[0] = i;
            pos[1] = j;

            // Lakukan perbandingan setiap baris dan kolom 
            // untuk mencari nilai minimum terhadap 
            // nilai minimum sebelumnya
            for (i = 1; i < m.NoRows; i++)
            {
                for (j = 0; j < m.NoCols - 1; j++)
                {
                    // jika nilai pada posisi baris i dan kolom j 
                    // lebih kecil dari nilai minimum saat ini
                    if (m[i, j] < minimum && m[i, j] > 0)
                    {
                        // jadikan nilainya adalah nilai minimum
                        // dan simpan posisi baris dan kolomnya
                        minimum = m[i, j];
                        pos[0] = i;
                        pos[1] = j;
                    }
                }
            }

            // kembalikan nilai posisi baris dan kolom 
            // untuk nilai minimum dari matrix m
            return pos;
        }
        #endregion

        #region Reset Form
        /// <summary>
        /// Event method untuk melakukan reset form
        /// </summary>
        private void btReset_Click(object sender, EventArgs e)
        {
            InitialValue();
            tbOutput.Clear();
        }
        #endregion
    }
}